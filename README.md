#NB-IoT开发板资料

“低功耗、强链接、新生态、大未来”，共建NB-IoT产业生态，驱动万物互联应用落地，中科创达携手华为推出全球首款NB-IoT开发板，旨在加速窄带物联网的快速普及。

NB-loT开发板搭载了Huawei LiteOS开源物联网操作系统和端云互通中间件，其为开发者提供了"一站式" 完整软件平台，向上支持不同的应用业务和算法需求，有效降低开发门槛、加速开发周期。

购买链接：https://item.taobao.com/item.htm?spm=a1z10.1-c.w4004-16846095592.3.26f161eaI4jlFz&id=556558974130

或扫描下方二维码：
![购买链接二维码](https://gitee.com/ts-nbiot/NB-IoT_Board/raw/master/pic/taobao.png)
